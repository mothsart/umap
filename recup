#!/usr/bin/env python3

import os
import datetime
import json
import subprocess

import requests


def get_archives():
    """Récupération de la liste des fichiers archivés dans l'ordre : du plus récent au plus vieux"""
    files_list = []
    if not os.path.isdir('archives'):
        os.mkdir('archives', 0o755)
        return files_list
    for args in os.walk("archives"):
        for _file in args[2]:
            files_list.append(_file)
    return sorted(files_list, reverse=True)


def get_map():
    """ On récupère le fichier json sur le site openstreetmap"""
    # on crée une session pour pouvoir partager les cookies entre plusieurs appels
    s = requests.Session()

    # On récupère le json décrivant l'ensemble des modifications sur la carte
    api_url = f'https://umap.openstreetmap.fr/map/115787/download/'
    raw = s.get(
        api_url
    ).text

    load_json = json.loads(raw)
    return json.dumps(
        load_json['layers'][0],
        sort_keys=True,
        indent=4
    )


def get_stream(files_list, index):
    """On récupère le contenu du dernier fichier archivé"""
    last_stream = None
    if files_list != []:
        last_file = files_list[index]
        with open('archives/' + last_file, 'r') as f:
            last_stream = f.read()
    return json.dumps(
        json.loads(last_stream),
        sort_keys=True,
        indent=4
    )

def get_last_stream(files_list):
    return get_stream(files_list, 0)

def set_two_nb(value):
    if value < 10:
        return '0%s' % value
    return value

def write_content(content):
    """On crée un nouveau fichier : année-mois-jour-heure.json afin que le tri des fichiers archivés soit simplifié"""
    today = datetime.datetime.now()
    file_name = '%s-%s-%s-%s.json' % (
        today.year,
        set_two_nb(today.month),
        set_two_nb(today.day),
        set_two_nb(today.hour)
    )
    with open('archives/' + file_name, 'w+') as f:
        f.write(content)
    return file_name


def parse_stat(response):
    """Récupère les données importantes du fichier"""
    stats = {
        "users": 0,
        "help_using": 0,
        "help_using_install": 0
    }
    _json = json.loads(response)
    for attr in _json['features']:
        if '_umap_options' not in attr['properties']:
            if 'name' not in attr['properties']:
                continue
            stats['users'] += 1
            continue
        if 'color' not in attr['properties']['_umap_options']:
            stats['users'] += 1
            continue
        color = attr['properties']['_umap_options']['color']
        if color == 'Green':
            stats['users'] += 1
            continue
        if color == 'Blue':
            stats['help_using'] += 1
            continue
        if color == 'Red':
            stats['help_using_install'] += 1
            continue
        stats['users'] += 1
    return stats


def edit_html(stats, files_list):
    if not stats:
        response = get_last_stream(files_list)
        stats = parse_stat(response)

    tbody = ''
    for index, f in enumerate(files_list):
        r = get_stream(files_list, index)
        s = parse_stat(r)
        tbody += '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>' % (
        f.replace('.json', ''),
        s['users'],
        s['help_using'],
        s['help_using_install']
    )

    """Création d'un contenu HTML a partir des données récupérées"""
    html = """
        <div>
            <p>
                <label>Nombre d'utilisateurs de Primtux (établissements principalement) :</label>
                <strong>%s</strong>
            </p>
            <p>
                <label>Nombre de personnes qui peuvent aider à l'utilisation : </label>
                <strong>%s</strong>
            </p>
            <p>
                <label>Nombre de personnes qui peuvent aider à l'utilisation et à l'installation :</label>
                <strong>%s</strong>
            </p>
            <table border="1">
                <thead>
                    <th>
                        Date
                    </th>
                    <th>
                        Nombre d'utilisateurs de Primtux (établissements principalement)
                    </th>
                    <th>
                        Nombre de personnes qui peuvent aider à l'utilisation
                    </th>
                    <th>
                        Nombre de personnes qui peuvent aider à l'utilisation et à l'installation
                    </th>
                </thead>
                <tbody>
                    %s
                </tbody>
            </table>
        </div>
    """ % (
        stats['users'],
        stats['help_using'],
        stats['help_using_install'],
        tbody
    )
    with open('stats.html', 'w+') as f:
        f.write(html)


def git_commit(file_name):
    """ On ajoute le fichier d'archive et on met à jour le fichier HTML produit"""
    subprocess.run(["git", "add", "."])
    subprocess.run(
        ["git", "commit", "-m", "create new %s" % file_name]
    )
    subprocess.run(["git", "push"])


if __name__ == '__main__':
    # on récupère les fichiers d'archives du plus récent au plus vieux
    files_list = get_archives()

    # on récupère le contenu (fichier json) de l'archive la plus récente
    last_stream = get_last_stream(files_list)
    # on récupère (fichier json) d'openstreetmap
    response = get_map()

    # on vérifie si le dernier enregistrement correspond à la valeur récupéré
    # dans le cas contraire, on interrompt le script
    if response == last_stream:
        print("aucun changements : ne rien faire")
        exit(1)

    # Le dernier contenu en ligne étant différent de la dernière version enregistrée, on enregistre le contenu dans un nouveau fichier d'archive
    file_name = write_content(response)

    # on récupère les infos importantes du site
    stats = parse_stat(response)

    # on met en forme dans un fichier HTML
    edit_html(stats, files_list)

    # on crée le commit correspondant aux changements et on le pousse sur le dépôt
    git_commit(file_name)
